package engine;

import java.util.ArrayList;

public class DatiBase {
	
	public String nomeCoppia;
	public String idCoppia;
	public String orizzonteTemporale;
	public double prezzoAttuale;
	public double prezzoPrecedente;
	public double volumeAttuale;
	public double volumePrecedente;
	public double tendenza;
	public double valoreRSIAttuale;
	public double valoreRSIPrecedente;
	
	public boolean ipercomprato;
	public boolean ipervenduto;
	public boolean divergenzaRialzista;
	public boolean divergenzaRibassista;
	public boolean coppiaVolatile;
	public boolean tendenzaPositiva;
	public boolean tendenzaNegativa;
	
	private double mediaVolumi;
	private double sogliaIpercomprato = 70.00;
	private double sogliaIpervenduto = 30.00;
	private double sogliaTendenzaPositiva = 5.00;
	private double sogliaTendenzaNegativa = -10.00;
	private boolean volumeUtile;
	
	
	public DatiBase(String nomeCoppia, String idCoppia, double tendenza, double prezzoAttuale,
			double volumeAttuale, double valoreRSIAttuale, String orizzonteTemporale) {
		
		this.nomeCoppia = nomeCoppia;
		this.idCoppia = idCoppia;
		this.tendenza = tendenza;
		this.prezzoAttuale = prezzoAttuale;
		this.volumeAttuale = volumeAttuale;
		this.valoreRSIAttuale = valoreRSIAttuale;
		this.orizzonteTemporale = orizzonteTemporale;
		this.tendenzaPositiva = tendenza > sogliaTendenzaPositiva;
		this.tendenzaNegativa = tendenza < sogliaTendenzaNegativa;
		calcoloMediaVolumi();
		calcoloVolatilitą();
		calcoloRSI();
		//calcoloMACD();
		//calcoloMedieMobili();
		//calcoloIndicatoreStocastico();
	}
	
	private void calcoloVolatilitą() {
		
		boolean coppiaVolatile = false;
		
		double valoreMassimo = 20.50;
		double valoreMinimo = 15.21;
		//double valoreMassimo = this.binanceService.getValoreMassimo(idCoppia);
		//double valoreMinimo = this.binanceService.getValoreMinimo(idCoppia);
		
		coppiaVolatile = (valoreMassimo - valoreMinimo) > (0.3*valoreMassimo);
		
		this.coppiaVolatile =  coppiaVolatile;
	}
	
	
	private void calcoloRSI() {
		
		boolean tendenzaPositiva = tendenza > 5.00? true:false;
		
		//Divergenze
		if(tendenzaPositiva && valoreRSIAttuale - valoreRSIPrecedente < 0) {
			divergenzaRibassista = true;
		}
		
		if(!tendenzaPositiva && valoreRSIAttuale - valoreRSIPrecedente > 0) {
			divergenzaRialzista = true;
		}
		
		//Iper
		if(valoreRSIAttuale > sogliaIpercomprato) {
			ipercomprato = true;
		}
		
		if(valoreRSIAttuale < sogliaIpervenduto) {
			ipervenduto = true;
		}
	}
	
	//Utile per avere una percezione di quanto siano alti i volumi rispetto allo storico
	private void calcoloMediaVolumi() {
		
		ArrayList<Double> volumi = new ArrayList<Double>();//this.binanceService.getVolumi(idCoppia)
		volumi.add(volumeAttuale);
		mediaVolumi = sommaVolumi(volumi)/volumi.size();
		
		if(volumeAttuale > (mediaVolumi + 5%mediaVolumi)) {
			setVolumeUtile(true);
		}
	}
	
	private double sommaVolumi(ArrayList<Double> volumi) {
		double result = 0.00;
		for(double volume : volumi) {
			result = result + volume;
		}
		
		return result;
	}

	public boolean isVolumeUtile() {
		return volumeUtile;
	}

	public void setVolumeUtile(boolean volumeUtile) {
		this.volumeUtile = volumeUtile;
	}
	
}
