package engine;

import java.util.ArrayList;
import java.util.List;

public class CryptocurrencyManager {
	
	
	private static CryptocurrencyManager instance = null;
	private List<DatiBase> cryptocurrencies = new ArrayList<DatiBase>();
	
	private CryptocurrencyManager() {}
	
	public static CryptocurrencyManager getSingleton(){
		if(instance == null) {
			return new CryptocurrencyManager();
		}else {
			return instance;
		}
	}

	public List<DatiBase> getCryptocurrencies() {
		return cryptocurrencies;
	}

	public void setCryptocurrencies(List<DatiBase> cryptocurrencies) {
		this.cryptocurrencies = cryptocurrencies;
	}
	
	public void setCryptocurrency(DatiBase cryptocurrency) {
		this.cryptocurrencies.add(cryptocurrency);
	}
}
