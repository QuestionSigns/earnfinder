package engine;

public class Main {
	
	public static boolean abilitaIndicatoreTendenza = true;
	public static boolean abilitaIndicatoreVolume = true;
	public static boolean abilitaIndicatoreRSI = true;
	
	public static int buy;
	public static int sell;
	
	public static int sogliaBuy = 2;
	public static int sogliaSell = 2;
	public double sogliaVolume = 500.00;
	
	public static void main(String[] args) {
		System.out.println("-------------------------------------------------");
		CryptocurrencyManager manager = CryptocurrencyManager.getSingleton();
		DatiBase crypto_XXX = new DatiBase("BNB/USDT", "123", 6.00, 450.32, 600.00, 73.00, "L");
		DatiBase crypto_YYY = new DatiBase("ADA/USDT", "123", -11.00, 1.98, 850.00, 73.00, "L");
		DatiBase crypto_ZZZ = new DatiBase("CRV/USDT", "123", 12.00, 450.32, 300.00, 73.00, "L");
		
		manager.setCryptocurrency(crypto_XXX);
		manager.setCryptocurrency(crypto_YYY);
		manager.setCryptocurrency(crypto_ZZZ);
		
		for(DatiBase crypto : manager.getCryptocurrencies()) {
//			==============================================================================			
			//Indicatore Tendenza
			if(abilitaIndicatoreTendenza) {
				if(crypto.tendenzaPositiva) {
					buy++;
				}
				if(crypto.tendenzaNegativa) {
					sell++;
				}				
			}
			
			//Indicatore Volume
			if(abilitaIndicatoreVolume) {
				if(crypto.isVolumeUtile()) {
					buy++;
					sell++;
				}
			}
			
			//Indicatore RSI
			if(abilitaIndicatoreRSI && !crypto.coppiaVolatile) {
				if(crypto.divergenzaRialzista) {
					buy++;
				}
				
				if(crypto.divergenzaRibassista) {
					sell++;
				}				
			}
			
//			==============================================================================
//			OUTPUT =======================================================================
			System.out.println(crypto.nomeCoppia + " buy totalizzati: " + buy);
			System.out.println(crypto.nomeCoppia + " sell totalizzati: " + sell);
			
			if(buy > sogliaBuy) {
				System.out.println("E' un buon momento per COMPRARE: " + crypto.nomeCoppia );
			}
			
			if(sell > sogliaSell) {
				System.out.println("E' un buon momento per VENDERE: " + crypto.nomeCoppia);
			}
			
			if(sell == buy) {
				System.out.println("Nessuna indicazione di mercato per: " + crypto.nomeCoppia);
			}
			System.out.println("-------------------------------------------------");
//			==============================================================================			
		}
	}
}
